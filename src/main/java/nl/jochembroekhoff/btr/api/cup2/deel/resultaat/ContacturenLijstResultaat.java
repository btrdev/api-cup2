/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel.resultaat;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.cup2.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;

/**
 *
 * @author jochem
 */
@Data
public class ContacturenLijstResultaat implements IResultaat<List<Contactuur>> {

    private boolean gelukt = false;
    private List<Contactuur> contacturen = new ArrayList<>();
    private SessieData sessieData;

    @Override
    public List<Contactuur> getResultaat() {
        return contacturen;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

}

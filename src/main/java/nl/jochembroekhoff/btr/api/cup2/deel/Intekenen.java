/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.Melding;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.IntekenenResultaat;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author jochem
 */
public class Intekenen {

    public static IntekenenResultaat intekenen(SessieData sessie, int kiesnummer) {

        boolean gelukt = true;
        Melding melding = null;

        Map<String, String> params = new HashMap<>();
        params.put("__EVENTARGUMENT", kiesnummer + "");
        params.put("__SCROLLPOSITIONX", "0");
        params.put("__SCROLLPOSITIONY", "0");
        params.put("lastPosition", "");
        params.put("txtLLtekst", "");
        params.put("hiddenlesnr", "");
        params.put("rememberClickY", "185");

        try (CloseableHttpResponse resp = ApiCup2.executeRequest(ApiCup2.maakRequest(sessie, "POST", "RoosterForm.aspx", true, false, params))) {

            Document doc = ApiCup2.getDocument(resp);

            System.out.println(doc);

            //fout is als er .prekolom>img is met onmouseout="hideHelpText();"
            Elements prekolomImg = doc.select(".prekolom > img");
            if (prekolomImg.size() > 0) {
                Element img = prekolomImg.first();
                if ("images/infoError.png".equals(img.attr("src"))
                        && img.hasAttr("onmouseover")) {
                    gelukt = false;
                    String bericht = img.attr("onmouseover").replace("showHelpText('", "").replace("',event);", "");
                    bericht = BtrApi.trimHtml(bericht);
                    melding = new Melding(bericht, false);
                }
            }

        } catch (URISyntaxException | IOException ex) {
            ex.printStackTrace();
        }

        return new IntekenenResultaat(gelukt, melding);
    }
}

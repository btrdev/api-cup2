/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel.resultaat;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.cup2.container.Melding;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;

/**
 *
 * @author jochem
 */
@Data
@RequiredArgsConstructor
public class IntekenenResultaat implements IResultaat {
    
    private final boolean gelukt;
    private final Melding melding;
    private SessieData sessieData;
    

    @Override
    public Object getResultaat() {
        return null;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

}

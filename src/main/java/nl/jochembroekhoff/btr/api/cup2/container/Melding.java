/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.container;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author jochem
 */
@Data
@RequiredArgsConstructor
public class Melding {
    private final String bericht;
    private final boolean positief;
}

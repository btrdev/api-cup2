/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.container;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author jochem
 */
@Data
@RequiredArgsConstructor
public class KeuzeOptie {
    private final Les les;
    private final boolean vol;
    private final int kiesnummer;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.container;

import lombok.Builder;
import lombok.Data;
import org.jsoup.nodes.Element;

/**
 *
 * @author jochem
 */
@Data
@Builder
public class Les {

    private final String vak;
    private final String lokaal;
    private final String docent;
    @Builder.Default
    private String extra = "";
    @Builder.Default
    private String mededeling = "";
    @Builder.Default
    private int plaatsen = -1;

    public static Les parseer(Element element) {
        return parseer(element, false);
    }

    public static Les parseer(Element element, boolean printbaarRoosterPatroon) {
        LesBuilder builder = Les.builder();
        String[] split = element.text().split(" ");

        //TODO: Mededeling
        String laatstBekendeExtra = "";

        int teller = 0;
        for (String deel : split) {
            if (deel.startsWith("[")
                    && deel.endsWith("]")
                    && deel.length() >= 3) {
                builder.plaatsen(Integer.parseInt(deel.substring(
                        1, deel.length() - 1)));
            } else {
                switch (teller) {
                    case 0:
                        if (printbaarRoosterPatroon) {
                            builder.docent(deel);
                        } else {
                            builder.vak(deel);
                        }
                        break;
                    case 1:
                        if (printbaarRoosterPatroon) {
                            builder.vak(deel);
                        } else {
                            switch (deel) {
                                case "toa":
                                case "deb":
                                    laatstBekendeExtra = deel;
                                    builder.extra(deel);
                                    break;
                                default:
                                    builder.lokaal(deel);
                                    break;
                            }
                        }
                        break;
                    case 2:
                        if (printbaarRoosterPatroon) {
                            switch (deel) {
                                case "toa":
                                case "deb":
                                    laatstBekendeExtra = deel;
                                    builder.extra(deel);
                                    break;
                                default:
                                    builder.lokaal(deel);
                                    break;
                            }
                        } else {
                            switch (laatstBekendeExtra) {
                                case "toa":
                                case "deb":
                                    builder.lokaal(deel);
                                    break;
                                default:
                                    builder.docent(deel);
                                    break;
                            }
                        }
                        break;
                    case 3:
                        switch (laatstBekendeExtra) {
                            case "toa":
                            case "deb":
                                if (printbaarRoosterPatroon) {
                                    builder.lokaal(deel);
                                } else {
                                    builder.docent(deel);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
                teller++;
            }
        }

        return builder.build();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.LogInResultaat;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.javatuples.Pair;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author jochem
 */
public class LogIn {

    public static LogInResultaat inloggen(SessieData sessie, String gebruiker, String pincode) {
        LogInResultaat lir = new LogInResultaat();

        Map<String, String> params = new HashMap<>();
        params.put("_nameDropDownList", gebruiker);
        params.put("_pincodeTextBox", pincode);
        params.put("_loginButton", "Login");

        try {
            Pair<HttpUriRequest, HttpClientContext> hreq = ApiCup2.maakRequest(sessie, "POST", "LogInWebForm.aspx", params);

            try (CloseableHttpResponse resp = ApiCup2.executeRequest(hreq)) {
                Document doc = ApiCup2.getDocument(resp);

                lir.setGelukt(true);

                if (doc.getElementById("_errorLabel") != null) {
                    Element errorLabel = doc.getElementById("_errorLabel");
                    if (!errorLabel.text().trim().equals("Error Label")) {
                        lir.setGelukt(false);
                        lir.setFoutmelding(errorLabel.text());
                    }
                }

                SessieData nieuweSessie = SessieData.zoekSessieDataUit(sessie, doc);

                nieuweSessie.getCookies().clear();
                nieuweSessie.getCookies().addAll(hreq
                        .getValue1()
                        .getCookieStore()
                        .getCookies());

                lir.setSessieData(nieuweSessie);

            }
        } catch (URISyntaxException | IOException ex) {
            lir.setGelukt(false);
            ex.printStackTrace();
        }

        return lir;
    }
}

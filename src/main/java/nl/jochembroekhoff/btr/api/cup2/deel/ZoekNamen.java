/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ZoekNamenResultaat;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.javatuples.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author jochem
 */
public class ZoekNamen {

    public static ZoekNamenResultaat zoekNamen(SessieData sessie, String zoekletters) {
        try {
            HttpResponse resp = ApiCup2.executeRequest(
                    ApiCup2.maakRequest(sessie, "GET", "Default.aspx", true, false, Collections.emptyMap()));
            sessie = SessieData.zoekSessieDataUit(sessie, resp.getEntity());

            Map<String, String> params = new HashMap<>();
            params.put("_nameTextBox", zoekletters);
            params.put("_zoekButton", "Zoek");
            params.put("numberOfLettersField", "3");

            HttpResponse resp2 = ApiCup2.executeRequest(
                    ApiCup2.maakRequest(sessie, "POST", "Default.aspx", params));
            String html = IOUtils.toString(resp2.getEntity().getContent(), Charset.defaultCharset());
            Document doc = Jsoup.parse(html);

            Map<String, String> namen = new HashMap<>();
            doc.select("#_nameDropDownList option")
                    .forEach(n -> namen.put(n.val(), n.text()));

            return new ZoekNamenResultaat(
                    SessieData.zoekSessieDataUit(sessie, doc),
                    namen);

        } catch (IOException | URISyntaxException ex) {
            ex.printStackTrace();
        }

        return new ZoekNamenResultaat(null, null);
    }
}

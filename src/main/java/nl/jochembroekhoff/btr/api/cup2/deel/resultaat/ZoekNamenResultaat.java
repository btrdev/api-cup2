/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel.resultaat;

import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;

/**
 *
 * @author jochem
 */
@RequiredArgsConstructor
public class ZoekNamenResultaat implements IResultaat {

    @Getter
    private final SessieData sessieData;

    @Getter
    private final Map<String, String> namen;

    @Override
    public Map<String, String> getResultaat() {
        return getNamen();
    }

    @Override
    public boolean gelukt() {
        return getSessieData() != null
                && getNamen() != null;
    }

}

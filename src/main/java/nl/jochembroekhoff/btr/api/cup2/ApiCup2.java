/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;
import lombok.Getter;
import lombok.Setter;
import nl.jochembroekhoff.btr.api.cup2.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup2.container.KeuzeOptie;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.ContacturenLijst;
import nl.jochembroekhoff.btr.api.cup2.deel.Intekenen;
import nl.jochembroekhoff.btr.api.cup2.deel.LogIn;
import nl.jochembroekhoff.btr.api.cup2.deel.PrintbaarRooster;
import nl.jochembroekhoff.btr.api.cup2.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ContacturenLijstResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.IntekenenResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.LogInResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.PrintbaarRoosterResultaat;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ZoekNamenResultaat;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.javatuples.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author jochem
 */
public class ApiCup2 {

    //Instellingen
    @Getter
    @Setter
    private static URI cupBasisUri;

    @Getter
    private static final CloseableHttpClient httpClient = HttpClients.createDefault();

    static {

        try {
            setCupBasisUri(new URIBuilder()
                    .setScheme("https")
                    .setHost("ccgobb.cupweb6.nl")
                    .build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SessieData getNieuweSessie() {
        try {
            SessieData sessie;

            HttpClientContext ctx = HttpClientContext.create();
            HttpGet hget = new HttpGet(getCupBasisUri());
            try (CloseableHttpResponse hres = getHttpClient().execute(hget, ctx)) {
                // Get current URI
                URI finalLocation = hget.getURI();
                List<URI> redirLocations = ctx.getRedirectLocations();
                if (redirLocations != null && redirLocations.size() > 0) {
                    finalLocation = redirLocations.get(redirLocations.size() - 1);
                }

                String token = finalLocation
                        .getPath()
                        .substring(1)
                        .replace("/default.aspx", "");
                sessie = SessieData.zoekSessieDataUit(new SessieData(token), hres.getEntity());
            }

            return sessie;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static Pair<HttpUriRequest, HttpClientContext> maakRequest(SessieData sessie, String method, String relatiefPad) throws URISyntaxException, UnsupportedEncodingException {
        return maakRequest(sessie, method, relatiefPad, true, true, Collections.emptyMap());
    }

    public static Pair<HttpUriRequest, HttpClientContext> maakRequest(SessieData sessie, String method, String relatiefPad, Map<String, String> overigeParams) throws URISyntaxException, UnsupportedEncodingException {
        return maakRequest(sessie, method, relatiefPad, true, true, overigeParams);
    }
    
    public static Pair<HttpUriRequest, HttpClientContext> maakRequest(SessieData sessie, String method, String relatiefPad, boolean metaVeldenToevoegen) throws URISyntaxException, UnsupportedEncodingException {
        return maakRequest(sessie, method, relatiefPad, metaVeldenToevoegen, true, Collections.emptyMap());
    }

    public static Pair<HttpUriRequest, HttpClientContext> maakRequest(SessieData sessie, String method, String relatiefPad, boolean metaVeldenToevoegen, boolean eventArgumentToevoegen, Map<String, String> overigeParams) throws URISyntaxException, UnsupportedEncodingException {
        if (relatiefPad.startsWith("/")) {
            relatiefPad = relatiefPad.substring(1);
        }

        URIBuilder uriBuilder = new URIBuilder(getCupBasisUri())
                .setPath(getCupBasisUri().getPath()
                        + "/" + sessie.getToken()
                        + "/" + relatiefPad);

        HttpUriRequest hreq = null;
        HttpClientContext ctx = HttpClientContext.create();

        if ("GET".equalsIgnoreCase(method)) {
            
            System.out.println("Preparing GET");

            if (metaVeldenToevoegen) {
                if (eventArgumentToevoegen) {
                    uriBuilder.setParameter("__EVENTARGUMENT", sessie.getEventArgument());
                }
                uriBuilder
                        .setParameter("__EVENTTARGET", sessie.getEventTarget())
                        .setParameter("__VIEWSTATE", sessie.getViewState())
                        .setParameter("__VIEWSTATEGENERATOR", sessie.getViewStateGenerator())
                        .setParameter("__EVENTVALIDATION", sessie.getEventValidation());
            }

            overigeParams.forEach((k, v) -> uriBuilder.setParameter(k, v));

            HttpGet hget = new HttpGet(uriBuilder.build());
            hreq = hget;
        } else if ("POST".equalsIgnoreCase(method)) {
            
            System.out.println("Preparing POST");

            HttpPost hpost = new HttpPost(uriBuilder.build());

            List<NameValuePair> formparams = new ArrayList<>();

            if (metaVeldenToevoegen) {
                formparams.add(new BasicNameValuePair("__EVENTTARGET", sessie.getEventTarget()));
                if (eventArgumentToevoegen) {
                    formparams.add(new BasicNameValuePair("__EVENTARGUMENT", sessie.getEventArgument()));
                }
                formparams.add(new BasicNameValuePair("__VIEWSTATE", sessie.getViewState()));
                formparams.add(new BasicNameValuePair("__VIEWSTATEGENERATOR", sessie.getViewStateGenerator()));
                formparams.add(new BasicNameValuePair("__EVENTVALIDATION", sessie.getEventValidation()));
            }

            overigeParams.forEach((k, v) -> {
                int index = 0;
                int foundAt = -1;
                for (NameValuePair nvp : formparams) {
                    if (nvp.getName().equals(k)) {
                        foundAt = index;
                    }
                    index++;
                }
                if (foundAt > -1) {
                    formparams.set(foundAt, new BasicNameValuePair(k, v));
                } else {
                    formparams.add(new BasicNameValuePair(k, v));
                }
            });
            
            hpost.setEntity(new UrlEncodedFormEntity(formparams));

            hreq = hpost;

        }

        // Add cookies
        CookieStore cookies = new BasicCookieStore();
        sessie.getCookies().forEach(cookie -> cookies.addCookie(cookie));
        ctx.setCookieStore(cookies);

        return Pair.with(hreq, ctx);
    }
    
    public static CloseableHttpResponse executeRequest(Pair<HttpUriRequest, HttpClientContext> request) throws IOException {
        return getHttpClient().execute(request.getValue0(), request.getValue1());
    }
    
    public static Document getDocument(HttpResponse hres) throws IOException {
        return Jsoup.parse(IOUtils.toString(hres.getEntity().getContent(), Charset.defaultCharset()));
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        /*
        Console console = System.console();
        if (console == null) {
            System.err.println("!! Geen console beschikbaar. Open het programma vanuit een terminal.");
            System.exit(-2);
        }
         */

        try {
            ApiCup2.setCupBasisUri(new URIBuilder()
                    .setScheme("https")
                    .setHost("ccgobb.cupweb6.nl")
                    .build());
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }

        System.out.println(">> WELKOM");
        System.out.println(">> Dit is BTR Api Cup2 [CLI]\n\n");

        System.out.println("Voer de eerste 3 tot 7 letters van je achternaam in:");
        String zoekletters = in.next().trim();

        System.out.println(">> Sessie initialiseren...");
        SessieData sessie = getNieuweSessie();

        System.out.println(">> Zoeken op " + zoekletters + " ...");
        ZoekNamenResultaat znr = ZoekNamen.zoekNamen(sessie, zoekletters);
        //ZoekNamenResultaat znr2 = ZoekNamen.zoekNamen(znr.getSessieData(), zoekletters);
        sessie = znr.getSessieData(); //Sessie vernieuwen

        if (znr.getNamen().size() < 1) {
            System.err.println("!! Voor deze zoekletters zijn geen namen gevonden. Probeer het opnieuw.");
            System.exit(-1);
        }

        List<String> naamKeys = new ArrayList(new TreeSet(znr.getNamen().keySet()));
        //Collections.sort(naamKeys);
        int i = 0;
        for (String naamKey : naamKeys) {
            System.out.println("[#" + i + "]: " + znr.getNamen().get(naamKey));
            i++;
        }
        System.out.println("Kies student #:");

        int keuzeIndex = in.nextInt();
        if (keuzeIndex > naamKeys.size() - 1) {
            System.err.println("!! Dat was geen optie!");
            System.exit(1);
        }
        String gekozenNaam = naamKeys.get(keuzeIndex);

        //String pincode = new String(console.readPassword("Voer pincode in en druk op Enter: [pincode wordt niet weergegeven]")).trim();
        System.out.println("Pincode:");
        String pincode = in.next().trim();

        LogInResultaat lir = LogIn.inloggen(sessie, gekozenNaam, pincode);
        if (!lir.gelukt()) {
            System.err.println("!! Niet ingelogd! Fout: " + lir.getFoutmelding());
            System.exit(2);
        } else {
            sessie = lir.getSessieData();
            System.out.println(">> Succesvol ingelogd!");
        }

        ContacturenLijstResultaat clr = ContacturenLijst.contacturenLijst(sessie);
        sessie = clr.getSessieData();

        int j = 0;
        for (Contactuur cu : clr.getContacturen()) {
            System.out.println("[#" + j + "]: " + cu.getDatum() + " (lesuur "
                    + cu.getLesuur() + ")");
            System.out.println("\tVoorgepland?: " + cu.isVoorgepland());
            System.out.println("\tHuidig: " + cu.getHuidigeInplanning());
            j++;
        }

        System.out.println("Wat wil je doen? Maak een keuze:");
        System.out.println("[i]: Intekenen bij een contactuur.");
        System.out.println("[p]: Printbaar rooster bekijken.");

        String keuze = in.next().trim();

        switch (keuze) {
            case "i":
                System.out.println("Kies contactuur om meer informatie over weer te geven:");
                int contactuurDetailIndex = in.nextInt();
                if (contactuurDetailIndex >= clr.getContacturen().size()) {
                    System.err.println("!! Dat was geen geldig contactuur.");
                    System.exit(3);
                }

                Contactuur cu = clr.getContacturen().get(contactuurDetailIndex);
                int k = 0;
                for (KeuzeOptie ko : cu.getKeuzeOpties()) {
                    System.out.println("[#" + k + "]: " + ko);
                    k++;
                }

                System.out.println("Waar wil je bij intekenen?: [Voer -1 in om te stoppen]");
                int keuzeOptieIndex = in.nextInt();
                if (keuzeOptieIndex < 0) {
                    System.out.println(">> Tot ziens.");
                    System.out.println(">> [Gemaakt door Jochem Broekhoff]");
                    System.exit(0);
                } else if (keuzeOptieIndex >= cu.getKeuzeOpties().size()) {
                    System.err.println("!! Dat was geen geldige keuzeoptie");
                    System.exit(4);
                }

                KeuzeOptie ko = cu.getKeuzeOpties().get(keuzeOptieIndex);
                IntekenenResultaat ir = Intekenen.intekenen(sessie, ko.getKiesnummer());
                if (ir.gelukt()) {
                    System.out.println(">> Intekenen succesvol.");
                    System.out.println(">> Tot ziens.");
                    System.out.println(">> [Gemaakt door Jochem Broekhoff]");
                    System.exit(0);
                } else {
                    System.err.println("!! Fout bij intekenen: [volgende regel(s)]");
                    System.err.println("[begin foutmelding]");
                    System.err.println(ir.getMelding().getBericht());
                    System.err.println("[einde foutmelding]");
                    System.exit(5);
                }
                break;
            case "p":
                PrintbaarRoosterResultaat prr = PrintbaarRooster.printbaarRooster(sessie);
                sessie = prr.getSessieData(); //Sessie vernieuwen

                prr.getResultaat().forEach(hk -> {
                    System.out.println(hk);
                });
                break;
        }
    }
}

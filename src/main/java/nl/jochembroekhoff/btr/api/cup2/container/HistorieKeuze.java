/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.container;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.joda.time.DateTime;

/**
 *
 * @author jochem
 */
@Data
@RequiredArgsConstructor
public class HistorieKeuze {
    private final DateTime datum;
    private final int lesuur;
    private final Les les;
}

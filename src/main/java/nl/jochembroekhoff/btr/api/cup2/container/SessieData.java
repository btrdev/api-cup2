/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.container;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.cookie.Cookie;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author jochem
 */
@RequiredArgsConstructor
@Data
public class SessieData implements Serializable {

    private final String token;
    private String eventTarget = "";
    private String eventArgument = "";
    private String viewState = "";
    private String viewStateGenerator = "";
    private String eventValidation = "";
    private final List<Cookie> cookies = new ArrayList<>();

    public static SessieData zoekSessieDataUit(SessieData huidigeSessieData, Document doc) {
        SessieData sessieData;
        try {
            sessieData = (SessieData) huidigeSessieData.clone();
        } catch (CloneNotSupportedException ex) {
            sessieData = new SessieData(huidigeSessieData.getToken());
        }

        Element eventTarget = doc.getElementById("__EVENTTARGET");
        Element eventArgument = doc.getElementById("__EVENTARGUMENT");
        Element viewState = doc.getElementById("__VIEWSTATE");
        Element viewStateGenerator = doc.getElementById("__VIEWSTATEGENERATOR");
        Element eventValidation = doc.getElementById("__EVENTVALIDATION");

        if (eventTarget != null) {
            sessieData.setEventTarget(eventTarget.val());
        }
        if (eventArgument != null) {
            sessieData.setEventArgument(eventArgument.val());
        }
        if (viewState != null) {
            sessieData.setViewState(viewState.val());
        }
        if (viewStateGenerator != null) {
            sessieData.setViewStateGenerator(viewStateGenerator.val());
        }
        if (eventValidation != null) {
            sessieData.setEventValidation(eventValidation.val());
        }

        return sessieData;
    }

    public static SessieData zoekSessieDataUit(SessieData huidigeSessieData, HttpEntity httpEntity) {
        try {
            String html = IOUtils.toString(httpEntity.getContent(), Charset.defaultCharset());
            Document doc = Jsoup.parse(html);
            return zoekSessieDataUit(huidigeSessieData, doc);
        } catch (IOException | UnsupportedOperationException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

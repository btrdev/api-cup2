/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel;

import java.util.HashMap;
import java.util.Map;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.HistorieKeuze;
import nl.jochembroekhoff.btr.api.cup2.container.Les;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.PrintbaarRoosterResultaat;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author jochem
 */
public class PrintbaarRooster {

    public static PrintbaarRoosterResultaat printbaarRooster(SessieData sessie) {
        PrintbaarRoosterResultaat res = new PrintbaarRoosterResultaat();

        boolean gelukt = true;

        try {
            try (CloseableHttpResponse hres = ApiCup2.executeRequest(ApiCup2.maakRequest(sessie, "GET", "RoosterForm.aspx", false))) {
                sessie = SessieData.zoekSessieDataUit(sessie, ApiCup2.getDocument(hres));
            }

            Map<String, String> params = new HashMap<>();
            params.put("ToPrintableRooster", "Printbaar Rooster");

            Element selectVan,
                    selectTot;
            String keuzeVan,
                    keuzeTot;

            try (CloseableHttpResponse hres = ApiCup2.executeRequest(ApiCup2.maakRequest(sessie, "POST", "RoosterForm.aspx", params))) {
                Document docStart = ApiCup2.getDocument(hres);

                selectVan = docStart.getElementById("dropDatumVan");
                selectTot = docStart.getElementById("dropDatumTot");
                keuzeVan = selectVan.select("> option").last().val();
                keuzeTot = selectTot.select("> option").first().val();

                sessie = SessieData.zoekSessieDataUit(sessie, docStart);
            }

            Map<String, String> params2 = new HashMap<>();
            params2.put("dropDatumVan", keuzeVan);
            params2.put("dropDatumTot", keuzeTot);

            try (CloseableHttpResponse hres = ApiCup2.executeRequest(ApiCup2.maakRequest(sessie, "POST", "PrintableRooster.aspx", params2))) {
                
                Document doc = ApiCup2.getDocument(hres);

                sessie = SessieData.zoekSessieDataUit(sessie, doc);

                Element printbaarTabel = doc.select(".StandaardTabel").first();
                if (printbaarTabel == null) {
                    gelukt = false;
                } else {
                    Elements rijen = printbaarTabel.select("> tbody > tr");

                    int iRij = 0;
                    int laatstBekendeWeekaanduiding = -1;
                    DateTime laatstBekendeDatum = null;
                    for (Element rij : rijen) {
                        if (iRij > 0) {
                            Elements kolommen = rij.select("> td");

                            if (kolommen.size() == 1) {
                                laatstBekendeWeekaanduiding = iRij;
                            } else if (laatstBekendeWeekaanduiding != (iRij - 1)) {
                                int iKolom = 0;
                                int iLesuur = 1;
                                for (Element kolom : kolommen) {
                                    if (iKolom == 0) {
                                        laatstBekendeDatum = BtrApi.DT_FORMATTER_JAAR_MAAND_DAG
                                                .parseDateTime(kolom.text()
                                                        .replace("\u00a0", ""));
                                    } else if (iKolom > 1) {
                                        if (kolom.text().length() > 1) {
                                            Les les = Les.parseer(kolom, true);
                                            res.getKeuzes().add(new HistorieKeuze(
                                                    laatstBekendeDatum, iLesuur, les));
                                        }

                                        iLesuur++;
                                    }

                                    iKolom++;
                                }
                            }
                        }

                        iRij++;
                    }
                }

                res.setSessieData(SessieData.zoekSessieDataUit(sessie, doc));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            gelukt = false;
        }

        res.setGelukt(gelukt);

        return res;
    }
}

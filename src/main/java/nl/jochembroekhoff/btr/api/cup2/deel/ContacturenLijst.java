/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup2.ApiCup2;
import nl.jochembroekhoff.btr.api.cup2.container.Contactuur;
import nl.jochembroekhoff.btr.api.cup2.container.KeuzeOptie;
import nl.jochembroekhoff.btr.api.cup2.container.Les;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;
import nl.jochembroekhoff.btr.api.cup2.deel.resultaat.ContacturenLijstResultaat;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.joda.time.DateTime;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author jochem
 */
public class ContacturenLijst {
    
    public static ContacturenLijstResultaat contacturenLijst(SessieData sessie) {
        ContacturenLijstResultaat clr = new ContacturenLijstResultaat();
        
        try {
            try (CloseableHttpResponse resp = ApiCup2.executeRequest(ApiCup2.maakRequest(sessie, "GET", "RoosterForm.aspx", false))) {
                
                Document doc = ApiCup2.getDocument(resp);
                
                clr.setSessieData(SessieData.zoekSessieDataUit(sessie, doc));

                //todo: testen of dhet wel echt goed is geladen
                Elements contacturen = doc
                        .select("#rooster > tbody > tr.cls0, "
                                + "#rooster > tbody > tr.cls1");
                
                contacturen.forEach(cu -> {
                    Elements cuKolommen = cu.select("> td");
                    int iterCuKolommen = 0;
                    
                    DateTime lesdatum = null;
                    int lesuur = -1;
                    Les huidigeInplanning = null;
                    boolean voorgepland = false;
                    List<KeuzeOptie> keuzeOpties = new ArrayList<>();
                    
                    for (Element cuKolom : cuKolommen) {
                        String text = cuKolom.text();
                        switch (iterCuKolommen) {
                            case 0: //lesdatum (9-1-2018)
                                lesdatum = BtrApi.DT_FORMATTER_JAAR_MAAND_DAG.parseDateTime(text);
                                break;
                            case 1: //lesuur (di2)
                                lesuur = Integer.parseInt(text.substring(2, 3));
                                break;
                            case 3: //gekozenles
                                if (cuKolom.hasClass("fixedLesson")) {
                                    voorgepland = true;
                                }
                                huidigeInplanning = Les.parseer(cuKolom);
                                break;
                            case 5: //keuzelessen
                                Elements keuzelessen = cuKolom
                                        .select(".choiceItem > .clsButton");
                                keuzelessen.forEach(keuzeles -> {
                                    boolean vol = !keuzeles.hasAttr("onmouseover")
                                            && !keuzeles.hasAttr("onmouseout");
                                    String onclick = keuzeles.attr("onclick").replace("keuzelesClicked(", "");
                                    int kommaindex = onclick.indexOf(",");
                                    int kiesnummer = Integer.parseInt(onclick.substring(0, kommaindex));
                                    KeuzeOptie ko = new KeuzeOptie(Les.parseer(keuzeles), vol, kiesnummer);
                                    keuzeOpties.add(ko);
                                });
                                break;
                            case 2: //roosterles (???)
                            case 4: //prekolom (bolletje+evt.foutmelding)
                            default:
                                break;
                        }
                        iterCuKolommen++;
                    }
                    
                    clr.getContacturen().add(new Contactuur(lesdatum, lesuur,
                            huidigeInplanning, voorgepland, keuzeOpties));
                });
            }
        } catch (URISyntaxException | IOException ex) {
            clr.setGelukt(false);
            ex.printStackTrace();
        }
        
        return clr;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.cup2.deel.resultaat;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.cup2.container.HistorieKeuze;
import nl.jochembroekhoff.btr.api.cup2.container.SessieData;

/**
 *
 * @author jochem
 */
@Data
public class PrintbaarRoosterResultaat implements IResultaat<List<HistorieKeuze>> {

    private boolean gelukt = false;
    private SessieData sessieData;
    private List<HistorieKeuze> keuzes = new ArrayList<>();

    @Override
    public List<HistorieKeuze> getResultaat() {
        return keuzes;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

}
